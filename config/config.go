package config

import (
	"bitbucket.org/sproutifyio/lib-utils/logger"
	"fmt"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

func SetConfig(configName string, configType string, configPath string) {

	viper.SetConfigName(configName) // name of config file (without extension)
	viper.SetConfigType(configType) // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(configPath) // path to look for the config file in
	viper.AddConfigPath(".")        // optionally look for config in the working directory
	err := viper.ReadInConfig()     // Find and read the config file
	if err != nil {                 // Handle errors reading the config file
		//logger.Fatalf("Fatal error config file: %s \n", err)
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		logger.Infof("Config file changed:", e.Name)
	})

}

type Config struct {
	Name         string
	Format       string
	Path         string
}

func (config *Config) parse() *Config {

	return config
}

