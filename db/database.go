package db

import (
	"bitbucket.org/sproutifyio/lib-utils/logger"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type DB struct {
	DbType   string
	Host     string
	Port     string
	User     string
	DbName   string
	Password string
	SslMode  string
}

func Connect(connectionDetails DB) *gorm.DB  {
	var connectionString = "host=" + connectionDetails.Host +
		" port=" + connectionDetails.Port +
		" user=" + connectionDetails.User +
		" dbname=" + connectionDetails.DbName +
		" password=" + connectionDetails.Password +
		" sslmode=" + connectionDetails.SslMode

	var db *gorm.DB
	var err error
	if db, err = gorm.Open(connectionDetails.DbType, connectionString); err != nil {
		logger.Errorf("failed to connect database %v", err)
		panic("failed to connect database")
	}

	return db
}
