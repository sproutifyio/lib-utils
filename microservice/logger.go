package microservice

import "bitbucket.org/sproutifyio/lib-utils/logger"

func (service *Microservice) Debug(fmt string, args ...interface{}){
	logger.Debugf(fmt, args...)
}

func (service *Microservice) Info(fmt string, args ...interface{}){
	logger.Infof(fmt, args...)
}

func (service *Microservice) Warn(fmt string, args ...interface{}){
	logger.Warningf(fmt, args)
}

func (service *Microservice) Error(fmt string, args ...interface{}){
	logger.Error(fmt, args)
}