package microservice

/**
	id is a hash of the Permission such that it can be
    queried via the user group permission table without having to query the actual permission table
    for the queries
 */
type Permission struct {
	 id string
	 Permission   string
	 Description  string
}

func GetPermissions(service *Microservice, userGroupId int) (*[]Permission, error) {
	var permissions []Permission

	if result := service.DB.Find(&permissions, " id = ?", userGroupId); result.Error == nil {
		return &permissions, nil
	}  else {
		// Return Empty Permission Set
		return &[]Permission{}, result.Error
	}
}