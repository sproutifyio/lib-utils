package microservice

import (
	"bitbucket.org/sproutifyio/lib-utils/db"
	"bitbucket.org/sproutifyio/lib-utils/logger"
	"bitbucket.org/sproutifyio/lib-utils/messaging"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"strings"
	"time"
)

type Microservice struct {
	ServiceName string
	Version     string
	Route       string
	Timeout     uint
	Handles     map[string]RequestHandler
	DB          *gorm.DB
	Logger      *os.File
	Heartbeat   *time.Ticker
}

type RequestHandler struct {
	route           string
	handle          func(service *Microservice, message Request)(response Response)
	permissions     []Permission
	requiredFields  []string
}

/**
	@arg version is passed from the build flags. The tag's are used to determine which version to build.
	this is additionally used when sending a heartbeat to the apigw for metrics and logging purposes.

 */
func Create(config string, configPath string, logFile *string, version string)(service *Microservice){

	viper.SetConfigName(config) // name of config file (without extension)
	viper.SetConfigType("json") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(configPath) // path to look for the config file in
	viper.AddConfigPath(".")        // optionally look for config in the working directory
	err := viper.ReadInConfig()     // Find and read the config file

	if err != nil {                 // Handle errors reading the config file
		//logger.Fatalf("Fatal error config file: %s \n", err)
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	var details = db.DB{
		DbType:   viper.GetString("database.dbtype"),
		Host:     viper.GetString("database.host"),
		Port:     viper.GetString("database.port"),
		User:     viper.GetString("database.user"),
		DbName:   viper.GetString("database.dbname"),
		Password: viper.GetString("database.password"),
		SslMode:  viper.GetString("database.sslmode"),
	}

	// Create & Initialize the Database
	_db := db.Connect(details)

	return &Microservice{
		ServiceName: viper.GetString("microservice.service"),
		Route:       viper.GetString("microservice.route"),
		Timeout:     uint(viper.GetInt("microservice.timeout")),
		Handles:     map[string]RequestHandler{},
		Version:     version,
		Logger:      logger.CreateLogger(*logFile+".log", viper.GetString("logging.level")),
		DB:          _db,
	}
}

func (service *Microservice) AddHandler(
	route string,
	handler func(service *Microservice, message Request)(response Response),
	permissions []Permission,
	requiredFields []string)(s *Microservice){

	var endpoint = strings.Replace(route, "/", "", -1)

	service.Handles[service.Route + "." + endpoint] = RequestHandler{endpoint, handler, permissions, requiredFields}
	service.Debug("Registered Route: %s", route)

	return service
}


func (service *Microservice) init(){
	// Create Permissions Table
	service.DB.Exec("CREATE TABLE IF NOT EXISTS permissions(id TEXT NOT NULL PRIMARY KEY, permission TEXT NOT NULL, description TEXT NOT NULL, created_at TIMESTAMP WITH TIME ZONE, updated_at TIMESTAMP WITH TIME ZONE);")

	// Create User Groups Table
	service.DB.Exec("CREATE SEQUENCE IF NOT EXISTS user_groups_id_seq START 2;")
	service.Debug("User Group ID Sequence Created")

	service.DB.Exec("CREATE TABLE IF NOT EXISTS user_groups(id INTEGER NOT NULL DEFAULT nextval('user_groups_id_seq'::regclass) PRIMARY KEY, user_group TEXT NOT NULL, description TEXT NOT NULL, created_at TIMESTAMP WITH TIME ZONE, updated_at TIMESTAMP WITH TIME ZONE);")
	service.Debug("User Group Table Created")

	userGroup := UserGroup{Id: 1, UserGroup: "Administrator", Description: "", CreatedAt: time.Now(), UpdatedAt: time.Now()}
	service.DB.Create(&userGroup)

	service.DB.Exec("CREATE TABLE IF NOT EXISTS user_group_permissions(user_group_id INTEGER NOT NULL, permission_id TEXT NOT NULL, assigned_by INTEGER NOT NULL);")
}

/**
	Goal of the Microservice is to Register with the APIGW and handle requests to the
	Indicated Route.
 */
func (service *Microservice) Run(init func(service *Microservice)) (s *Microservice) {
	service.init()
	init(service)

	service.Debug("Registered Routes: %v",  service.Handles)

	defaultTimeout, _ := time.ParseDuration(fmt.Sprintf("%sms", "200"))
	if viper.IsSet("microservice.timeout"){
		if timeout, parseErr := time.ParseDuration(fmt.Sprintf("%sms", viper.GetString("microservice.timeout"))); parseErr == nil {
			service.Debug("Using Timeout: %i", timeout.Milliseconds())
			messaging.SetRequestTimeout(timeout)
		} else {
			service.Debug("Using Default Timeout: %s", "200")
			messaging.SetRequestTimeout(defaultTimeout)
		}
	} else {
		logger.Debugf("Using Default Timeout: %s", "200")
		messaging.SetRequestTimeout(defaultTimeout)
	}
	messaging.CreateEncodedConn(viper.GetString("nats.host"), viper.GetString("nats.port"))

	var request = messaging.NatsMessage{
		Data:  map[string]interface{}{
			"Route": service.Route,
			"Timeout": service.Timeout,
			"ServiceName": service.ServiceName,
		},
		Error: "",
	}

	if response := messaging.Request("register", request); response.Error == "" {
		service.Info("Microservice Registered Successfully")

		for subject, handler := range service.Handles {
			var _subject = subject
			service.Info("Subscribing to Subject: %s Handler: %s", _subject, handler.route)

			messaging.Subscribe(subject, func(message messaging.NatsMessage) messaging.NatsMessage {
				//TODO: Handle authorization via GateKepper Functionality
				_handler := service.Handles[_subject]

				var _request Request
				req, _ := json.Marshal(message.Data)
				var response Response
				if err := json.Unmarshal(req, &_request); err == nil {

					service.Debug("Message Received for Subject: %s Handler: %s", _subject , _handler.route)
					service.Debug("Invoking function handler: %s", _handler.route)
					//Required Field Checks

					hasRequiredParameters := true
					fieldValidationMessage := ""
					for _, required := range _handler.requiredFields {
						if _request.Params[required] == nil {
							hasRequiredParameters = false
							fieldValidationMessage = fmt.Sprintf("Required Field: %s", required)
						}
					}

					if hasRequiredParameters {

						//TODO: check token expiry

						if len(_handler.permissions) > 0 && _request.Token == nil {
							response = UnauthorizedRequestException()
						} else {
							response = _handler.handle(service, _request)
						}

					} else {
						response = InvalidParameterException(fieldValidationMessage)
					}

					response.TID = _request.TID
					service.Debug("Response: %v", response)

					return messaging.NatsMessage{Data: response}
				} else {
					response = Response{
						TID: _request.TID,
						Result: nil,
						Error: &ErrResponse{
							ErrorCode: http.StatusInternalServerError,
							Message:   fmt.Sprintf( "Failed to Decode Request Struct: %v", _request),
							Error:     errors.New("NatsDecodingException"),
						},
					}

					service.Debug("Response: %v", response)
					return messaging.NatsMessage{Data: response, Error: "NatsDecodingException"}
				}
			})
		}

		service.Info("Start Heartbeat Mechanism")
		service.Heartbeat = time.NewTicker(5 * time.Second)

		go func(){
			for {
				select {
				case t := <-service.Heartbeat.C:
					service.Debug("Heartbeat: %v", t)
				}
			}}()

	} else {
		service.Info(fmt.Sprintf("Registration Exception: %v", response.Error))
		panic(response.Error)
	}

	return service
}

func (service *Microservice) Shutdown(){
	service.DB.Close()
	service.Logger.Close()
	service.Heartbeat.Stop()
}