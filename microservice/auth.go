package microservice

import (
	"bitbucket.org/sproutifyio/lib-utils/logger"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"strings"
	"time"
)

//TODO: Add IP into the Token to check for Cross Site Scripting Attacks
//TODO: Add Session Information to the JWT Token

var salt []byte = []byte("sproutifyio@!?)*")

type Token struct {
	UserId       int
	Permissions  []Permission
	Exp          time.Time
}

func (token *Token) getPermissions() []Permission {
	return token.Permissions
}

func (token *Token) getUserID() int {
	return token.UserId
}

func (token *Token) getExpiry() time.Time {
	return token.Exp
}

/*@code{Authenticate} Responsible for generating the authentication token*/
func Authenticate(service *Microservice, userId int) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": userId,
		"exp": time.Now().Add(time.Hour * time.Duration(24)),
		"permissions": []Permission{},
	})

	if signedToken, err := token.SignedString(salt); err == nil {
		return signedToken, nil
	} else {
		return "", err
	}
}

/* @code{ParseToken}
   Validate given a jwt checks to see if the user session is valid
   and returns the user_id and user_group_id such that the authentication
   process can retrieve the permissions */

func ParseToken(jwtToken string) (*Token, error) {
	if jwtToken == "" {
		return nil, errors.New("invalid jwt token")
	}

	jwtToken = strings.TrimSpace(jwtToken)

	token, err := jwt.Parse(jwtToken, func (token *jwt.Token)(interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid jwt token")
		}
		return salt, nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var authToken Token

		_claims, _ := json.Marshal(claims)
		if unmarshalErr := json.Unmarshal(_claims, &authToken); unmarshalErr == nil {
			logger.Debug("Token: %v", authToken)
			return &authToken, nil
		} else {
			logger.Debug(fmt.Sprintf("Exception: %v", unmarshalErr))
			return nil, unmarshalErr
		}

	} else {
		return nil, errors.New("invalid jwt token")
	}
}