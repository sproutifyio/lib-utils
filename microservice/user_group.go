package microservice

import "time"

type UserGroup struct {
	Id          int
	UserGroup   string
	Description string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type UserGroupPermission struct {
	UserGroupId   int
	PermissionId  string
	AssignedBy    int
}