package microservice

import (
	"bitbucket.org/sproutifyio/lib-utils/messaging"
	"time"
)

type heartbeat struct {
	timestamp time.Time
	microservice string
	version string
}

func (service *Microservice) Beat() string {
	beat := messaging.NatsMessage{Data: heartbeat{
		timestamp: time.Now(),
		microservice: service.ServiceName,
		version: service.Version,
	}}

	if response := messaging.Request("heartbeat", beat); response.Error == "" {
		return response.Data.(string)
	}

	return ""
}

